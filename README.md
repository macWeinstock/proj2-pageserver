### Project 2

- Author: Mac Weinstock

- Contact: mweinsto@uoregon.edu

- Project2: Created a trivial server with Flask python web framework which searches for valid HTML/CSS files within a specified directory using a url, including the following functionality

  1. If URL ends with name.html or name.css (i.e., if path/to/name.html is in document path (from DOCROOT)), send content of name.html or name.css with proper http response.

  2. If name.html is not in current directory Respond with 404 (not found). 

  3. If a url contains one of the symbols(~ // ..), respond with 403 forbidden error. For example, url=localhost:5000/..name.html or /~name.html would give 403 forbidden error.