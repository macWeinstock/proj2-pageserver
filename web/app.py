from flask import Flask, render_template, request, abort
import os

#Got a lot of help from here: https://flask.palletsprojects.com/en/1.1.x/api/
#As well as here for understanding decorators: https://www.programiz.com/python-programming/decorator
#And help from Piazza @41, @42, @43

app = Flask(__name__)

#for the initial page (localhost:5000) return a page not found error
@app.route("/")
def index():
	abort(404)

#route for the trivia.html page, as long as the url is valid
#found code for cwd here: https://stackoverflow.com/questions/5137497/find-current-directory-and-files-directory
@app.route("/<name>")
def trivia(name):
	cwd = os.getcwd()

	#I don't think this is the best way to solve this problem, but
	#in order to render valid .css OR valid .html files this was
	#my first run at a working solution

	#Quick error check and abort if .html or .css is not in url
	if ".html" not in name and ".css" not in name:
		abort(404)
	#if .html is in the url and the path exists to the file, render it
	if ".html" in name:
		source_path = os.path.join("/templates/", name)
		if os.path.exists(cwd + source_path):
			return render_template(name)
	#if .css is in the url and the path exists to the file, render it
	if ".css" in name:
		source_path = os.path.join("/static/styles", name)
		if os.path.exists(cwd + source_path):
			return render_template(name)
	#otherwise the .html/.css file does not exist
	abort(404)
	
#decorator for 403 error
@app.errorhandler(403)
def error_403(e):
	return render_template("403.html"), 403

#decorator for 404 error with check for a 403 error (the error we want to implement
#in flask automatically shoots to a 404, but we want a 403, so had to implement another
#check)
@app.errorhandler(404)
def error_404(e):
	url = request.path
	if "~" in url or "//" in url or ".." in url:
		f = error_403(e)
		return f
	else:
		return render_template("404.html"), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')